 const Product = require("../models/product");








// -=-=-=-=-=-=-=-=-=-=-=-=-=-=- START OF MINIMUM REQUIREMENTS -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-==--=-==--=-==--=-==--=-==--=-==--=-==--=-==--=-==--=-==--=-==--=-==--=-= //



// I. ====================>>>>>>>>>> USER ONLY (PRODUCT CATALOG PAGE) <<<<<<<<<<<<<<============================== //


// A. =============== Retrieve / GET ALL ACTIVE PRODUCTS ======== //

module.exports.getAllActiveProduct = (data) => {
    
	return Product.find({isActive: true}).then(result => {
   		 return result;
    });

}

// ==================================================== //




// B. ================ Retrieve single Product =================== //


module.exports.retrieveSingleProduct = (data) => {
	if (data.isAdmin) {
	  return Product.findById(data.id).then((product) => {
		return product;
	  });
	}
	let message = Promise.resolve("User must be Admin to use this feature.");
  
	return message.then((value) => {
	  return value;
	});
  };
  
  
  
  // ============================================================ //

// ====================>>>>>>>>>> END of USER ONLY (PRODUCT CATALOG PAGE) <<<<<<<<<<<<<<============================== //














// II. ====================>>>>>>>>>> ADMIN DASHBOARD <<<<<<<<<<<<<<============================== //

// A. =================================== CREATE / ADD PRODUCT ================================================= //
module.exports.addProduct = (reqBody) => {
	let newProduct = new Product({
	  name: reqBody.productName,
	  description: reqBody.productDescription,
	  price: reqBody.price,
	//   productImage: reqBody.productImage,
	  createdOn: Date(),
	});
  
	return newProduct.save().then((product, error) => {
	  if (error) {
		return false;
	  } else {
		return product;
	  }
	});
  };

// ======================================================================================= // 








// B. =============== Retrieve / GET ALL PRODUCTS ======== //
module.exports.getAllProduct = (data) => {

	return Product.find({}).then(result => {

		return result;

	});


}

// =================================================== //







// C. ========== UPDATE PRODUCT INFORMATION ======================================= //

module.exports.updateProduct = (reqParams, reqBody) => {

	// specify the fields/properties of the document to be updated


	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};


		return Product.findByIdAndUpdate(reqParams.productId,updatedProduct).then((course, error) => {

				if (error) {

					return false;

				} else {

					return {
						message: "Product updated successfully"
					}
				};


		});

};
// ================================================================================================== //









// D. -=-=-=-=-=-=-=-=-=-=-=- DEACTIVATE / REACTIVATE PRODUCT -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //



// 1. =============================== DEACTIVATE / ARCHIVE PRODUCT ============================================================= //

module.exports.deactivate = (reqParams) => {

	let updateActiveField = {
		
		isActive : false
		
	};

		return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
			if (error) {

				return false;
			} else {
				return {
					message : "deactivate Product successfully"
				}
			};
		});

};
// ================================================================================================================ //








// 2. =============================== REACTIVATE / UNARCHIVE PRODUCT ============================================================= //

module.exports.reactivate = (reqParams) => {

	let updateActiveField = {
		
		isActive : true
	};

		return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
			if (error) {

				return false;
			} else {
				return {
					message : "reactivate Product successfully"
				}
			};
		});

};
// ================================================================================================================ //

// -=-=-=-=-=-=-=-=-=-=-=- END of DEACTIVATE / REACTIVATE PRODUCT -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //


// ========================>>>>>>>>>> END of ADMIN DASHBOARD <<<<<<<<<<<<<<============================== //







// E. ============================= CHECKOUT ORDER (Non - Admin User checkout (Create Order) ) ======================================== //

module.exports.userCheckOut = (data) => {

	return Product.findById(data.product.productId)
	.then(product => {

		if(data.isAdmin){

			let message = Promise.resolve({
					message: 'Admin cannot place an order'
			})
			return message
			.then((value) => {
				return value
			})
		}


			let newOrder = new Order({
					userId: data.userId,
					products: [{
							productId: data.product.productId,
							quantity: data.product.quantity
					}],
					totalAmount: product.price * data.product.quantity
			});

				return newOrder.save().then(result => {
					return Promise.resolve({
								message: "Order successfully created",
								result: result
					})
			})

	})
};


// ======================================================================================================================== //

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=- END OF MINIMUM REQUIREMENTS -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=--=-=-=-=-=--=-=-=-=-=--=-=-=-=-=--=-=-=-=-=--=-=-=-=-=--=-=-=-=-=- //











// Retrieve User Details
// =================== RETRIEVE ACTIVE PRODUCTS ================================== //


module.exports.retrieveUserDetails = () => {

	return Product.find({isActive : true}).then(result => {
		return result;
	}) ;
};
// ====================================================================== //










