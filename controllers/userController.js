// ========= INSTALLERS & LOCALHOST URLS ========== //

// const Orders = require("../models/orders");
const Course = require("../models/product");
const User = require("../models/user");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// ================================================== //









// ======================== CHECK EMAIL EXISTS ======================================= //

module.exports.checkEmailExists = (reqBody) =>
{
    return User.findOne({email : reqBody.email}).then(result => {

        if ((result = true) && (result !== reqBody.email) ) {


                return{
                    message: "User was found"
                        }
                 }
         if (result.length > 1)
                 {

                return {
                    message: "Duplicate User"
                        }
                }
         else {

          // return false
                return {
                    message:"User not found"
                         }
                }
    })    
}

// ===================================================================== //







// -=-=-=-=-=-=-=-=-=-=-=-=-=-=- START OF MINIMUM REQUIREMENTS -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-==--=-==--=-==--=-==--=-==--=-==--=-==--=-==--=-==--=-==--=-==--=-==--=-==--=-= //



// =========================== REGISTER PAGE ========================================= //

module.exports.registerUser = (reqBody) => 
{
    let newUser = new User ({

        firstName:  reqBody.firstName,
        lastName:   reqBody.lastName,
        email:      reqBody.email,
        mobileNo:   reqBody.mobileNo,

        password:   bcrypt.hashSync(reqBody.password, 10)
                            })


        return newUser.save().then
        (
                (user, error)=>
             {

                if (error) {

                    return false;

                           }

                 else {

                    return user

                      }

             }

        );
}

// ====================================================================================== //











// =============== LOGIN PAGE =================================================== //

module.exports.loginUser = (reqBody) => {

    return User.findOne({email : reqBody.email})
    
    .then(result => {

        if(result == null ){

            return {

                message: "Sorry, This User is Not found in our database."

            }

        } else {


            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);


            if (isPasswordCorrect) {

                return {access : auth.createAccessToken(result)}

            } else {

                // if password doesn't match
                return {

                    message: "Your Password was incorrect!"

                }

            };
        };
    });
};

// ============================================================================================ //




// -=-=-=-=-=-=-=-=-=-=-=-=-=-=- END OF MINIMUM REQUIREMENTS -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-==--=-==--=-==--=-==--=-==--=-==--=-==--=-==--=-==--=-==--=-==--=-==--=-==--=-==--=-= //

















// ====================== DETAILS OF THE USER ========================================== //

module.exports.detailsUser = (reqBody) => {

    return User.find({_id : reqBody.id})
    
    .then(user => {
        
      
            return user
        

        })
    }


module.exports.product = async (data) => {

							// async = to make to functions sa "data" //asynchronous = isa2
							// .await


    let isUserUpdated = await User.findById(data.userId)
    
    .then(user =>{

        user.product.push({productId : data.productId});

        return user.save()
        
    .then((user, error)=>{

            if (error) {

                     return false;

            } else {

                return true;

             };

        });

    });






    let isProductUpdated = await Product.findById(data.productId)
    
    .then(product => {


        course.product.push({productId : data.productId});

        return product.save()
        
    .then((product, error) => {

            if (error) {

                return false;

            } else {

                return true;
            };

        });

    });


    if(isUserUpdated && isProductUpdated){

        return {

            message : "You are now Registered!"
        }
 

    } else {

        return false;
    };

}

