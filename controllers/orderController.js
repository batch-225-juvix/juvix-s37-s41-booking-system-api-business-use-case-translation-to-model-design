const Product = require("../models/order");
// ========================================================= //



module.exports.addOrders = (data) => {
	if(data.isAdmin){
		let new_orders = new Orders ({
			name: data.orders.name,
			description: data.orders.description,
			price: data.orders.price
		})

		return new_orders.save().then((new_orders, error) => {
			if(error){
				return false
			}

			return {
				message: 'New orders successfully created!'
			}
		})
	} 


									// Restriction (Admin only) //
	let message = Promise.resolve('User must me an Admin to Access this Order.')

	return message.then((value) => {
		return value


	})
		
}
// ======================================================================================= // 








// =============== RETRIEVING ALL ORDERS () ======== //

module.exports.getAllOrders = () => {
    return Orders.find({}).then(result => {
    return result;
    });

};
// =================================================== //









// ========================= UPDATE ORDERS ======================================= //

module.exports.updateOrders = (reqParams, reqBody) => {

	// specify the fields/properties of the document to be updated


	let updatedOrders = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};


		return Orders.findByIdAndUpdate(reqParams.ordersId,updatedOrders).then((course, error) => {

				if (error) {

					return false;

				} else {

					return {
						message: "Orders updated successfully"
					}
				};


		});



};

// ================================================================================================== //









// ===============================ARCHIVE ORDERS =================================================================== //

module.exports.archiveOrders = (reqParams) => {

	let updateActiveField = {
		
		isActive : false // sa schema nka default = true, para inactive isActive = false
	};

		return Orders.findByIdAndUpdate(reqParams.ordersId, updateActiveField).then((orders, error) => {
			if (error) {

				return false;
			} else {
				return {
					message : "Archiving Orders successfully"
				}
			};
		});

};
// ================================================================================================================ //









// =================== RETRIEVE ACTIVE ORDERS ================================== //


module.exports.getAllActive = () => {

	 return Course.find({isActive : true}).then(result => {
	 	return result;
	 }) ;
};
// ====================================================================== //
