// ========= INSTALLER ============= //

const mongoose = require("mongoose");

// ================================= //





// option 1 //
// ======== SCHEMA ================ //

const userSchema = new mongoose.Schema({


    email: {
        
        type: String,
        required: [true, "Email name is required"]

    },

    password: {
        
        type: String,
        required: [true, "password name is required"]

    },

    isAdmin: {
        
        type: Boolean,
        default: false

    },

});

// ===================================================== //







// =========== CONSOLE LOG ======================== //
module.exports = mongoose.model("user", userSchema);
// ================================================ //