// ====== INSTALLER ================= //

const mongoose = require("mongoose");

// ================================== //




// ========== SCHEMA for orders=============== //

const orderSchema = new mongoose.Schema({
  userId: {
    type: String,
  },
  products: [
    {
      productId: {
        type: String,
      },

      quantity: {
        type: Number,
      },
    },
  ],
  totalAmount: {
    type: Number,
    default: 1,
  },
  purchasedOn: {
    type: Date,
    default: new Date(),
  },
});


// ============================================================== //





// =========CONSOLE LOG for Orders ===========//

module.exports = mongoose.model("Order", orderSchema);


// ======================================================== //