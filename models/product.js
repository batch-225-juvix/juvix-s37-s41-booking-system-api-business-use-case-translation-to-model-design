// ====== INSTALLER ================= //

const mongoose = require("mongoose");

// ================================== //





// ========== SCHEMA for products=============== //

const productSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "Name is required"]
	},
	description : {
		type : String,
		required : [true, "Description is required"]
	},
	price : {
		type : Number,
		required : [true, "Price is required"]
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		default : new Date()
	},

	quantity : {
		type : Number,
		default : true
	},

	// totalPrice : {
	// 	type: Number,
	// 	default: [true, "Your total ammount"]
	// },


})

// ============================================================== //




// =========CONSOLE LOG for Product ======================//

module.exports = mongoose.model("product", productSchema);

// ==================================================== //