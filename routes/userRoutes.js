// // ======== INSTALLERS FOR ROUTES ======== //

const express = require("express");
const router = express.Router();
const auth = require("../auth");

// ================================= //





// ============== IMPORT =================================== //

const userController = require("../controllers/userController")

// =========================================================== //










// -=-=-=-=-=-=-=-=-=-=-=-=-=-=- START OF MINIMUM REQUIREMENTS -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //





// ============== LOCALHOST URL ROUTES FOR REGISTRATION ===================== //

router.post("/register", (req, res) => {

     userController.registerUser(req.body).then(
         
          result => res.send(result))

});

// =================================================================== //




// ============== LOCALHOST URL ROUTES FOR CHECK EMAIL ==================== //

router.post("/checkEmail", (req, res) => {
     userController.checkEmailExists(req.body).then(result => res.send(result));
});

// =================================================================== //





// ======= LOCALHOST URL ROUTES FOR      LOGIN/AUTHENTICATION ==================== //

router.post("/login", (req, res) => {

     userController.loginUser(req.body).then(result => res.send(result));
})

// ============================================================ //







// ==== CONSOLE LOG ====== //

module.exports = router;

// ============================ //