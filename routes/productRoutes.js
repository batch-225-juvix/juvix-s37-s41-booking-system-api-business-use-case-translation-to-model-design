
// ====== INSTALLERS ============== //
const express = require("express");
const router = express.Router();
const auth = require("../auth");

// ================================= //



// ==================Connection to the productControllers.js=================== //
// for directory
const productController = require("../controllers/productController");
// ============================================= //



// ============== LOCALHOST URL FOR PRODUCT =============== //

/* router.post("/product", auth.verify, (req, res) => {

     let data = {
       // user ID will be retrieved from the request header
       userId : auth.decode(req.headers.authorization).id,
       
       // Course ID will be retrieve from the request body
       courseId : req.body.courseId
     }
   
     userController.enroll(data).then((result) => res.send(result));
   
   })
 */
// =============================================================== //










// -=-=-=-=-=-=-=-=-=-=-=-=-=-=- START OF MINIMUM REQUIREMENTS -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= //


// I. ====================>>>>>>>>>> USER ONLY (PRODUCT CATALOG PAGE) <<<<<<<<<<<<<<============== //


// A. ============  RETRIEVE ALL ACTIVE PRODUCT ====================== //

router.get("/retrieveAllActiveProducts", auth.verify, (req, res) => {

 const data = {
        product: req.body,
        isActive: true
 }   
    productController.getAllActiveProduct(data).then((result) => res.send(result));
   
  });


// ===================================================================== //



// B. ======== RETRIEVE SINGLE PRODUCT ===============!!!! //


router.post("/retrieveSingleProduct", auth.verify, (req, res) => {
  const data = {
    id: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  productController.retrieveSingleProduct(data).then((result) => res.send(result));
});


// ================================================= //





// ==============>>>>>>>>>>>>>>>>>>END of USER ONLY (PRODUCT CATALOG PAGE)<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<=================== //









// II. ================>>>>>>>>> ADMIN DASHBOARD <<<<<<<<<<<<<==================== //

// A. ============== LOCALHOST URL ROUTES FOR CREATE / ADD PRODUCT (ADMIN ONLY) ===================== //
 router.post("/createProduct", (req, res) => {
   productController.addProduct(req.body).then((result) => res.send(result));
  });
   


// ===================================================================== //






// B. ========= LOCALHOST URL ROUTES FOR RETRIEVE ALL PRODUCT (ADMIN ONLY) ================== //

router.get("/", (req, res) => { 

	productController.getAllProduct().then(result => res.send(result));

}); 
 

 // ================================================================ //







 // III. ==================== UPDATE PRODUCT I.D. (ADMIN ONLY) =========================================== //
router.put("/updating/:productId", auth.verify, (req, res) => {

	productController.updateProducts(req.params, req.body)
  .then((result) => res.send(result));
});
// ====================================================================================== //








//  IV. -=-=-=-=-=-=-=-=-=-=-=- DEACTIVATE / REACTIVATE PRODUCT -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //

// A. ======= LOCALHOST URL ROUTES FOR ARCHIVING A PRODUCT (SOFT DELETE ITEMS)============ //

router.put("/deactivate/:productId", auth.verify, (req, res) => {

	productController.deactivateProduct(req.params).then(result => res.send(result));
});






/* router.put("/deactivate/:productId", auth.verify, (req, res) => {
  const adminKey = { isAdmin: auth.decode(req.headers.authorization).isAdmin };

  productController
    .deactivate(req.params, adminKey)
    .then((result) => res.send(result));
}); */
// ============================================================================================ //





// B. ======= LOCALHOST URL ROUTES FOR UNARCHIVING A PRODUCT (RESTORE DELETE ITEMS)============ //

router.put("/reactivate/:productId", auth.verify, (req, res) => {

  productController.reactivate(req.params)
  .then(
    (result) => res.send(result)
  );
});
// =============================================================================================== //

//  -=-=-=-=-=-=-=-=-=-=-=- End of DEACTIVATE / REACTIVATE PRODUCTS -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //






// ===================>>>>>>>>>>>>>>>>>>>>>>>END of ADMIN DASHBOARD<<<<<<<<<<<<<<<<<<<<<<<<<<================================== //








// V. =========== Non Admin User CheckOut (Create Order) ========= //

router.get("/userCheckout", auth.verify, (req, res) => {

  productController.userCheckout(req.params).then(

    (result) => res.send(result)
    );
});




// =============================================================== //

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=- END OF MINIMUM REQUIREMENTS -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //





// === console log ========== //
module.exports = router;
// ========================== //
